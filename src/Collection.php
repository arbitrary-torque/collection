<?php

namespace ArbitraryTorque;

use InvalidArgumentException;

/**
 * Class Collection
 * @package App
 */
class Collection implements CollectionInterface
{
    /**
     * @var \ArrayIterator
     */
    protected $_collection;
    /**
     * @var string|null
     */
    private $_itemClass = null;
    /**
     * @var int|null
     */
    private $_itemCount = null;

    /**
     * @param array $collection
     */
    public function __construct(array $collection = [])
    {
        $this->_collection = new \ArrayIterator();
        # Create our internal array iterator, so we can proxy to it's methods easily
        foreach ($collection as $item) {
            $this->append($item);
        }
    }

    /**
     * @param $item
     *
     * @return $this
     */
    public function append($item)
    {
        # Throw exception if item class is set and item is not instance of that class
        $this->_validateItemClass($item);

        $this->_validateCount();

        $this->_collection->append($item);

        return $this;
    }

    /**
     * Determines if we're at the last item
     *
     * @return bool
     */
    public function isLast() : bool
    {
        return (($this->key() + 1) == count($this));
    }

    /**
     * Determines if we've got the first item
     *
     * @return bool
     */
    public function isFirst() : bool
    {
        return ($this->key() == 0);
    }

    /**
     * @param \Closure $closure
     *
     * @return CollectionInterface
     */
    public function each(\Closure $closure) : CollectionInterface
    {
        foreach ($this as $itemKey => $item) {
            call_user_func($closure, $item, $itemKey);
        }

        return $this;
    }

    /**
     * @param \Closure $closure
     *
     * @return CollectionInterface
     */
    public function filter(\Closure $closure) : CollectionInterface
    {
        # Static here over self, so it returns the correct empty collection in subclasses
        $collection = new static();

        $this->each(function($item, $key) use ($collection, $closure) {

            if (call_user_func($closure, $item, $key))
                $collection->append($item);

        });

        return $collection;
    }

    /**
     * @param \Closure|null $closure
     *
     * @return mixed|null
     */
    public function first(\Closure $closure = null)
    {
        if (is_callable($closure)) {
            foreach ($this as $key => $item) {
                # If we pass in a function, we'll evaluate it and return the item
                # if we get true returned from user function
                if (call_user_func($closure, $item, $key))
                    return $item;
            }
        } else {
            # Otherwise just grab the first
            $copy = $this->_collection->getArrayCopy();
            return array_shift($copy); # assigning to avoid reference error
        }

        return null;
    }

    /**
     * @param \Closure|null $closure
     *
     * @return mixed
     */
    public function last(\Closure $closure = null)
    {
        # TODO Maybe just cycle in reverse??
        if (is_callable($closure)) {

            $items = [];

            foreach ($this as $key => $item) {
                # If we pass in a function, we'll evaluate it and store it in
                # a collection of valid items
                if (call_user_func($closure, $item, $key))
                    $items[] = $item;
            }

            return array_pop($items);
        } else {
            # Otherwise just grab the last
            $copy = $this->_collection->getArrayCopy();
            return array_pop($copy); # assigning to avoid reference error
        }
    }

    /**
     * @param $item
     *
     * @return CollectionInterface
     */
    public function push($item) : CollectionInterface
    {
        # Throw exception if item class is set and item is not instance of that class
        $this->_validateItemClass($item);

        $this->_validateCount();

        $copy = $this->_collection->getArrayCopy();

        array_push($copy, $item);

        $this->_collection = new \ArrayIterator($copy);

        return $this;
    }

    /**
     * @return mixed
     */
    public function pop()
    {
        $copy = $this->_collection->getArrayCopy();

        $item = array_pop($copy);

        $this->_collection = new \ArrayIterator($copy);

        return $item;
    }

    /**
     * @param $item
     *
     * @return CollectionInterface
     */
    public function unshift($item) : CollectionInterface
    {
        # Throw exception if item class is set and item is not instance of that class
        $this->_validateItemClass($item);

        $this->_validateCount();

        $copy = $this->_collection->getArrayCopy();

        array_unshift($copy, $item);

        $this->_collection = new \ArrayIterator($copy);

        return $this;
    }

    /**
     * @return mixed
     */
    public function shift()
    {
        $copy = $this->_collection->getArrayCopy();

        $item = array_shift($copy);

        $this->_collection = new \ArrayIterator($copy);

        return $item;
    }

    /**
     * @param \Closure $closure
     *
     * @return bool
     */
    public function all(\Closure $closure) : bool
    {
        $allItemsMatch = true;

        $this->each(function($item, $key) use (&$allItemsMatch, $closure) {

            if ( ! call_user_func($closure, $item, $key)) {
                $allItemsMatch = false;
                return false;
            }
        });

        # Using array copy here because I think it will perform better
        return (count($this->_collection->getArrayCopy())) ? $allItemsMatch : false;
    }

    /**
     * @param \Closure $closure
     *
     * @return bool
     */
    public function any(\Closure $closure) : bool
    {
        $anyItemMatches = false;

        $this->each(function($item, $key) use (&$anyItemMatches, $closure) {

            if (call_user_func($closure, $item, $key)) {
                $anyItemMatches = true;
                return false;
            }

        });

        # Using array copy here because I think it will perform better
        return (count($this->_collection->getArrayCopy())) ? $anyItemMatches : false;
    }

    /**
     * @param \Closure $closure
     *
     * @return bool
     */
    public function none(\Closure $closure) : bool
    {
        return ! $this->any($closure);
    }

    /**
     * @param CollectionInterface $collection
     *
     * @return CollectionInterface
     */
    public function diff(CollectionInterface $collection) : CollectionInterface
    {
        $diff = array_diff($this->toArray(), $collection->toArray());
        # Static here over self, so it returns the correct empty collection in subclasses
        return new static($diff);
    }

    /**
     * @param string $column
     *
     * @return CollectionInterface
     */
    public function column(string $column) : CollectionInterface
    {
        $collection = new static();

        $this->each(function ($item) use ($collection, $column) {

            if ( is_object($item) && method_exists($item,'toArray')) {
                $item = $item->toArray();
            }

            if ($item instanceof \stdClass) {
                $item = (array)$item;
            }

            if ( is_array($item) ) {

                if (array_key_exists($column, $item) && $item[$column])
                    $collection->append($item[$column]);

            } else if ($item instanceof \ArrayAccess) {

                if ($item->offsetExists($column) && $item[$column])
                    $collection->append($item[$column]);

            } else {
                trigger_error('Unable to use column collection method on non-array or object that does\'t implement toArray method', E_USER_WARNING);
            }
        });

        return $collection;
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public function in($value) : bool
    {
        return $this->any(function ($currentValue) use ($value) { return $currentValue == $value; });
    }

    /**
     * Determines whether the current iteration is even
     *
     * @return bool
     */
    public function even() : bool
    {
        return ((int)$this->key() % 2 == 0);
    }

    /**
     * @return bool
     */
    public function odd() : bool
    {
        return  ! $this->even();
    }

    /**
     * @return bool
     */
    public function toArray() : array
    {
        $arrayCopy = $this->_collection->getArrayCopy();
        $newArray  = [];

        foreach($arrayCopy as $item) {
            if ( is_object($item) && method_exists($item, 'toArray')) {
                $item = $item->toArray();
            }
            $newArray[] = $item;
        }

        return $newArray;
    }

    /**
     * Implements Countable
     *
     * @return int
     */
    public function count()
    {
        return count($this->_collection);
    }

    /**
     * Implements Iterator
     *
     * @access public
     */
    public function rewind()
    {
        $this->_collection->rewind();
    }

    /**
     * Implements Iterator
     *
     * @access public
     * @return mixed
     */
    public function current()
    {
        return $this->_collection->current();
    }

    /**
     * Implements Iterator
     *
     * @access public
     * @return int The current row number
     */
    public function key()
    {
        return $this->_collection->key();
    }

    /**
     * Gets the key in human readable format, ie. starting at 1
     * Paginated, if the collection is, ie. 51-100
     *
     * @return int
     */
    public function getHumanKey() : int
    {
        $keys = range(1, count($this));

        return $keys[$this->key()];
    }

    /**
     * Implements Iterator
     *
     * @access public
     */
    public function next()
    {
        $this->_collection->next();
    }

    /**
     * Implments Iterator
     *
     * @access public
     * @return boolean If the next row is a valid row.
     */
    public function valid()
    {
        return $this->_collection->valid();
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        # Throw exception if item class is set and item is not instance of that class
        $this->_validateItemClass($value);

        # If we're not replacing an existing item, we'll validate count
        if ( ! $this->offsetExists($offset)) {
            $this->_validateCount();
        }

        $this->_collection->offsetSet($offset, $value);
    }

    /**
     * @param mixed $offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        if ( ! $this->offsetExists($offset))
            return null;

        return $this->_collection->offsetGet($offset);
    }

    /**
     * @param mixed $offset
     *
     * @return bool
     */
    public function offsetExists($offset)
    {
        return $this->_collection->offsetExists($offset);
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset)
    {
        $this->_collection->offsetUnset($offset);
    }

    /**
     * @param string|null $itemClass
     *
     * @return $this
     */
    public function setItemClass(?string $itemClass): self
    {
        $this->_itemClass = $itemClass;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getItemClass(): ?string
    {
        return $this->_itemClass;
    }

    /**
     * @param int|null $itemCount
     *
     * @return $this
     */
    public function setItemCount(?int $itemCount) : self
    {
        $this->_itemCount = $itemCount;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getItemCount(): ?int
    {
        return $this->_itemCount;
    }

    /**
     * @param $item
     */
    private function _validateItemClass($item)
    {
        $itemClass = $this->getItemClass();

        # Only validate if we have an item class. Legacy, and some generic classes, won't have one.
        if ($itemClass) {
            if ( ! $item instanceof $itemClass)
                throw new InvalidArgumentException(self::class . ' expects value to be ' . $itemClass . ' ' . ( is_object($itemClass) ? get_class($item) : (string)$item ) . ' given');
        }
    }

    /**
     * @param $item
     */
    private function _validateCount()
    {
        $maxItems = $this->getItemCount();

        # Only validate if we have an item class. Legacy, and some generic classes, won't have one.
        if ($maxItems) {
            if (count($this) + 1 > $maxItems)
                throw new InvalidArgumentException(self::class . ' only allows ' . $maxItems  . ($this->getItemClass() ? ' of ' . $this->getItemClass() : ' items'));
        }
    }
}