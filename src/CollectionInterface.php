<?php 

namespace ArbitraryTorque;

/**
 * Interface CollectionInterface
 * @package App
 */
interface CollectionInterface extends \Iterator, \Countable, \ArrayAccess
{
    /**
     * @param \Closure $closure
     *
     * @return self
     */
    public function each(\Closure $closure) : CollectionInterface;

    /**
     * @param \Closure $closure
     *
     * @return self
     */
    public function filter(\Closure $closure) : CollectionInterface;

    /**
     * @param \Closure|null $closure
     *
     * @return mixed|null
     */
    public function first(\Closure $closure = null);

    /**
     * @param \Closure|null $closure
     *
     * @return mixed
     */
    public function last(\Closure $closure = null);

    /**
     * @param \Closure $closure
     *
     * @return bool
     */
    public function all(\Closure $closure) : bool;

    /**
     * @param \Closure $closure
     *
     * @return bool
     */
    public function any(\Closure $closure) : bool;

    /**
     * @param \Closure $closure
     *
     * @return bool
     */
    public function none(\Closure $closure) : bool;

    /**
     * @param CollectionInterface $collection
     *
     * @return self
     */
    public function diff(CollectionInterface $collection) : CollectionInterface;

    /**
     * @param string $column
     *
     * @return CollectionInterface
     */
    public function column(string $column): CollectionInterface;

    /**
     * @param $value
     *
     * @return bool
     */
    public function in($value) : bool;

    /**
     * @return mixed
     */
    public function isFirst() : bool;

    /**
     * @return mixed
     */
    public function isLast() : bool;

    /**
     * Determines whether the current iteration is even
     *
     * @return bool
     */
    public function even() : bool;

    /**
     * @return bool
     */
    public function odd() : bool;

    /**
     * @return bool
     */
    public function toArray() : array;

    /**
     * Gets the key in human readable format, ie. starting at 1
     * Tends to be paginated on paginated collections
     *
     * @return int
     */
    public function getHumanKey() : int;
}
