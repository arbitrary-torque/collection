<?php 

namespace ArbitraryTorque\Collection\Tests;

use ArbitraryTorque\Collection;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use stdClass;

/**
 * Class CollectionTest
 * @package Tests\Unit
 */
class CollectionTest extends TestCase
{
    /**
     * @group unit
     *        collection
     */
    public function testCount()
    {
        $collection = new Collection([1,2,3,4]);

        $this->assertEquals(4, count($collection));

        $collection = new Collection(range(1,150));

        $this->assertEquals(150, count($collection));
    }

    /**
     * @group unit
     *        collection
     */
    public function testAppend()
    {
        $collection = new Collection([1,2,3,4]);

        $collection->append(5);

        $this->assertEquals(5, count($collection));

        $this->assertEquals(5, $collection->last());
    }

    /**
     * @group unit
     *        collection
     */
    public function testAppendClass()
    {
        # For sub-classes, they'll have to override the methods or, more preferably, just set in constructor.
        $collection = new Collection();
        $collection->setItemClass(stdClass::class);

        $object = new stdClass();

        $collection->append($object);

        $this->assertEquals(1, count($collection));

        $this->assertInstanceOf(stdClass::class, $collection->last());
    }

    /**
     * @group unit
     *        collection
     */
    public function testAppendClassFails()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('ArbitraryTorque\Collection expects value to be stdClass 1 given');

        # For sub-classes, they'll have to override the methods or, more preferably, just set in constructor.
        $collection = new Collection();
        $collection->setItemClass(stdClass::class);

        $collection->append(1);
    }

    /**
     * @group unit
     *        collection
     */
    public function testAppendCount()
    {
        # For sub-classes, they'll have to override the methods or, more preferably, just set in constructor.
        $collection = new Collection();
        $collection->setItemCount(2);

        $collection->append(1);
        $collection->append(2);

        $this->assertEquals(2, count($collection));
        $this->assertEquals(2, $collection->last());
    }

    /**
     * @group unit
     *        collection
     */
    public function testAppendCountFails()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('ArbitraryTorque\Collection only allows 2 items');

        # For sub-classes, they'll have to override the methods or, more preferably, just set in constructor.
        $collection = new Collection();
        $collection->setItemCount(2);

        $collection->append(1);
        $collection->append(2);
        $collection->append(3);
    }

    /**
     * @group unit
     *        collection
     */
    public function testAppendCountFailsClassName()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('ArbitraryTorque\Collection only allows 2 of stdClass');

        # For sub-classes, they'll have to override the methods or, more preferably, just set in constructor.
        $collection = new Collection();
        $collection->setItemCount(2);
        $collection->setItemClass(stdClass::class);

        $object1 = new stdClass();
        $object2 = new stdClass();
        $object3 = new stdClass();

        $collection->append($object1);
        $collection->append($object2);
        $collection->append($object3);
    }

    /**
     * @group unit
     *        collection
     */
    public function testIsLast()
    {
        $collection = new Collection([1,2,3,4]);

        foreach ($collection as $i) {
            $this->assertTrue((($i == 4 && $collection->isLast()) || $i != 4 && ! $collection->isLast()));
        }
    }

    /**
     * @group unit
     *        collection
     */
    public function testIsFirst()
    {
        $collection = new Collection([1,2,3,4]);

        foreach ($collection as $i) {
            $this->assertTrue((($i == 1 && $collection->isFirst()) || $i != 1 && ! $collection->isFirst()));
        }
    }

    /**
     * @group unit
     *        collection
     */
    public function testEach()
    {
        $numbers     = [1,2,3,4];
        $testNumbers = [];
        $collection  = new Collection($numbers);

        $collection->each(function($item, $key) use (&$testNumbers) {
            $testNumbers[$key] = $item;
        });

        $this->assertEquals($numbers, $testNumbers);
    }

    /**
     * @group unit
     *        collection
     */
    public function testFilter()
    {
        $collection = new Collection([1,2,3,4]);

        $numbersLessThanFour = $collection->filter(function($i) {
            return $i < 4;
        });

        $this->assertInstanceOf('ArbitraryTorque\Collection', $numbersLessThanFour);

        $this->assertEquals(new Collection([1,2,3]), $numbersLessThanFour);
    }

    /**
     * @group unit
     *        collection
     */
    public function testFirst()
    {
        $collection = new Collection([1,2,3,4]);

        $this->assertEquals(1, $collection->first());
    }

    /**
     * @group unit
     *        collection
     */
    public function testFirstWithClosure()
    {
        $collection = new Collection([1,2,3,4]);

        $this->assertEquals(3, $collection->first(function ($i) { return $i > 2; }));
    }

    /**
     * @group unit
     *        collection
     */
    public function testLast()
    {
        $collection = new Collection([1,2,3,4]);

        $this->assertEquals(4, $collection->last());
    }

    /**
     * @group unit
     *        collection
     */
    public function testPush()
    {
        $collection = new Collection([3,2]);

        $collection->push(1);
        $collection->push(0);

        $this->assertSame(
            [
                3, 2, 1, 0
            ],
            $collection->toArray()
        );

        $collection->push(12345);

        $this->assertSame(
            [
                3, 2, 1, 0, 12345
            ],
            $collection->toArray()
        );
    }

    /**
     * @group unit
     *        collection
     */
    public function testPop()
    {
        $collection = new Collection([3,2]);

        $this->assertSame(
            2,
            $collection->pop()
        );

        $this->assertSame(
            [
                3
            ],
            $collection->toArray()
        );
    }

    /**
     * @group unit
     *        collection
     */
    public function testUnshift()
    {
        $collection = new Collection([3,2]);

        $collection->unshift(1);
        $collection->unshift(0);

        $this->assertSame(
            [
                0, 1, 3, 2
            ],
            $collection->toArray()
        );

        $collection->unshift(12345);

        $this->assertSame(
            [
                12345, 0, 1, 3, 2
            ],
            $collection->toArray()
        );
    }

    /**
     * @group unit
     *        collection
     */
    public function testShift()
    {
        $collection = new Collection([3,2]);

        $this->assertSame(
            3,
            $collection->shift()
        );

        $this->assertSame(
            [
                2
            ],
            $collection->toArray()
        );
    }

    /**
     * @group unit
     *        collection
     */
    public function testLastWithClosure()
    {
        $collection = new Collection([1,2,3,4]);

        $this->assertEquals(2, $collection->last(function ($i) { return $i < 3; }));
    }

    /**
     * @group unit
     *        collection
     */
    public function testAll()
    {
        $collection = new Collection([1,2,3,4]);

        $this->assertTrue($collection->all(function ($i) { return $i < 5; }));
        $this->assertFalse($collection->all(function ($i) { return $i < 4; }));
    }

    /**
     * @group unit
     *        collection
     */
    public function testAny()
    {
        $collection = new Collection([1,2,3,4]);

        $this->assertTrue($collection->any(function ($i) { return $i == 3; }));
        $this->assertFalse($collection->any(function ($i) { return $i > 5; }));
    }

    /**
     * @group unit
     *        collection
     */
    public function testNone()
    {
        $collection = new Collection([1,2,3,4]);

        $this->assertTrue($collection->none(function ($i) { return $i > 5; }));
        $this->assertFalse($collection->none(function ($i) { return $i == 3; }));
    }

    /**
     * @group unit
     *        collection
     */
    public function testToArray()
    {
        $collection = new Collection(
            [
                1,
                2,
                3,
                4,
                new class {
                    public function toArray()
                    {
                        return [5,6];
                    }
                }
            ]
        );

        $this->assertEquals([1,2,3,4,[5,6]], $collection->toArray());
    }

    /**
     * @group unit
     *        collection
     */
    public function testDiff()
    {
        $collection1 = new Collection([1,2,3,4]);
        $collection2 = new Collection([1,2]);

        $difference  = $collection1->diff($collection2);

        $this->assertInstanceOf('ArbitraryTorque\Collection', $difference);

        $this->assertEquals(new Collection([3,4]), $difference);
    }

    /**
     * @group unit
     *        collection
     */
    public function testColumn()
    {
        $collection = new Collection([
            [
                'id'   => 1,
                'name' => 'Bob'
            ],
            [
                'id'   => 2,
                'name' => 'John'
            ],
            [
                'id'   => 3,
                'name' => 'Phil'
            ],
            [
                'id'   => 4,
                'name' => 'Alice'
            ]
        ]);

        $ids = $collection->column('id');

        $this->assertEquals(
            new Collection([1,2,3,4]),
            $ids
        );

        $names = $collection->column('name');

        $this->assertEquals(
            new Collection(['Bob', 'John', 'Phil', 'Alice']),
            $names
        );

        $frogs = $collection->column('frogs');

        $this->assertEquals(
            new Collection(),
            $frogs
        );
    }

    /**
     * @group unit
     *        collection
     */
    public function testColumnStdClass()
    {
        $items = [
            [
                'id'   => 1,
                'name' => 'Bob'
            ],
            [
                'id'   => 2,
                'name' => 'John'
            ],
            [
                'id'   => 3,
                'name' => 'Phil'
            ],
            [
                'id'   => 4,
                'name' => 'Alice'
            ]
        ];

        foreach ($items as &$item) {
            $item = (object)$item;
        }

        $collection = new Collection($items);

        $ids = $collection->column('id');

        $this->assertEquals(
            new Collection([1,2,3,4]),
            $ids
        );

        $names = $collection->column('name');

        $this->assertEquals(
            new Collection(['Bob', 'John', 'Phil', 'Alice']),
            $names
        );

        $frogs = $collection->column('frogs');

        $this->assertEquals(
            new Collection(),
            $frogs
        );
    }

    /**
     * @group unit
     *        collection
     */
    public function testColumnArrayAccess()
    {
        $collection = new Collection([

            new \ArrayIterator(
                [
                    'id'   => 1,
                    'name' => 'Bob'
                ]
            ),

            new \ArrayIterator(
                [
                    'id'   => 2,
                    'name' => 'John'
                ]
            ),

            new \ArrayIterator(
                [
                    'id'   => 3,
                    'name' => 'Phil'
                ]
            ),

            new \ArrayIterator(
                [
                    'id'   => 4,
                    'name' => 'Alice'
                ]
            )
        ]);

        $ids = $collection->column('id');

        $this->assertEquals(
            new Collection([1,2,3,4]),
            $ids
        );

        $names = $collection->column('name');

        $this->assertEquals(
            new Collection(['Bob', 'John', 'Phil', 'Alice']),
            $names
        );

        $frogs = $collection->column('frogs');

        $this->assertEquals(
            new Collection(),
            $frogs
        );
    }

    /**
     * @group unit
     *        collection
     */
    public function testColumnToArray()
    {
        $collection = new Collection([

            new class {
                public function toArray()
                {
                    return [
                        'id'   => 1,
                        'name' => 'Bob'
                    ];
                }
            },

            new class {
                public function toArray()
                {
                    return [
                        'id'   => 2,
                        'name' => 'John'
                    ];
                }
            },

            new class {
                public function toArray()
                {
                    return [
                        'id'   => 3,
                        'name' => 'Phil'
                    ];
                }
            },

            new class {
                public function toArray()
                {
                    return [
                        'id'   => 4,
                        'name' => 'Alice'
                    ];
                }
            }
        ]);

        $ids = $collection->column('id');

        $this->assertEquals(
            new Collection([1,2,3,4]),
            $ids
        );

        $names = $collection->column('name');

        $this->assertEquals(
            new Collection(['Bob', 'John', 'Phil', 'Alice']),
            $names
        );

        $frogs = $collection->column('frogs');

        $this->assertEquals(
            new Collection(),
            $frogs
        );
    }

    /**
     * @group unit
     *        collection
     */
    public function testColumnFailTriggerWarning()
    {
        $this->expectException('PHPUnit\Framework\Error\Warning');
        $this->expectExceptionMessage('Unable to use column collection method on non-array or object that does\'t implement toArray method');


        $collection = new Collection([
            'fun', 'weee', 'yeah', 'great'

        ]);

        $ids = $collection->column('id');
    }

    /**
     * @group unit
     *        collection
     */
    public function tesIn()
    {
        $collection = new Collection([1,2,3,4]);

        $this->assertTrue($collection->in(2));
        $this->assertFalse($collection->in(5));
    }

    /**
     * @group unit
     *        collection
     */
    public function testIterator()
    {
        $collection = new Collection([1,2,3,4]);
        $compare = 1;

        $collection->rewind();

        while ($collection->valid()) {
            $this->assertEquals($compare, $collection->current());
            $this->assertEquals(($compare - 1), $collection->key());

            $collection->next();

            $compare += 1;
        }

        $collection->rewind();

        $compare = 1;

        while ($collection->valid()) {
            $this->assertEquals($compare, $collection->current());
            $this->assertEquals(($compare - 1), $collection->key());

            $collection->next();

            $compare += 1;
        }
    }

    /**
     * @group unit
     *        collection
     */
    public function testEven()
    {
        $collection = new Collection([1,2,3,4]);

        foreach ($collection as $i) {

            # test for position not value - starts at 0
            if (in_array($i, [1,3])) {

                $this->assertTrue($collection->even());

            } else {

                $this->assertFalse($collection->even());

            }
        }
    }

    /**
     * @group unit
     *        collection
     */
    public function testOdd()
    {
        $collection = new Collection([1,2,3,7]);

        # test for position not value - starts at 0
        foreach ($collection as $i) {

            if (in_array($i, [2,7])) {

                $this->assertTrue($collection->odd());

            } else {

                $this->assertFalse($collection->odd());

            }
        }
    }

    /**
     * @group unit
     *        collection
     */
    public function testArrayAccess()
    {
        $collection = new Collection(['Thing', 'Stuff']);

        $collection[2] = 'Item';

        $this->assertEquals('Thing', $collection[0]);
        $this->assertEquals('Stuff', $collection[1]);
        $this->assertEquals('Item', $collection[2]);

        unset($collection[1]);

        $this->assertNull($collection[1]);

        $this->assertEquals(2, count($collection));

        $this->assertEquals('Thing', $collection[0]);
        $this->assertEquals('Item', $collection[2]);
    }

    /**
     * @group unit
     *        collection
     */
    public function testHumanKeyNoPagination()
    {
        $numbers     = [1,2,3,4];
        $testNumbers = [];
        $collection  = new Collection($numbers);

        $collection->each(function($item, $key) use (&$testNumbers, $collection) {
            $testNumbers[] = $collection->getHumanKey();
        });

        $this->assertEquals($numbers, $testNumbers);
    }
}
